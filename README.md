# libsdrplay_api

Packaging of SDRPlay's binary only drivers from their website https://www.sdrplay.com/
Based on the Beta 3.07 drivers.

This driver should support all their devices including the RSPdx and RSPduo.

API documentation: https://www.sdrplay.com/docs/SDRplay_API_Specification_v3.06.pdf

## Installation from debian/ubuntu repository

The driver is split in two packages, libsdrplay-api with only the runtime library and service,
and libsdrplay-api-dev with the header files.

To install it from my repo:

There is a repository with binaries for amd64 (PCs), i386 (old PCs), armhf (Raspberry and other arm boards running in 32bit) and arm64 (Raspberry and other arm boards running in 64 bit) for several Raspbian, Debian, Ubuntu and Linux Mint distributions. Supported distributions/architectures are:

| Release                       | i386               | amd64              | armhf              | arm64              |
|-------------------------------|--------------------|--------------------|--------------------|--------------------|
| Debian/Raspbian 9 (Stretch)   |                    |                    | :heavy_check_mark: |                    |
| Debian/Raspbian 10 (Buster)   |                    |                    | :heavy_check_mark: |                    |
| Debian/Raspbian 11 (Bullseye) |                    |                    | :heavy_check_mark: |                    |
| Debian 9 (Stretch)            | :heavy_check_mark: | :heavy_check_mark: |                    | :heavy_check_mark: |
| Debian 10 (Buster)            | :heavy_check_mark: | :heavy_check_mark: |                    | :heavy_check_mark: |
| Debian 11 (Bullseye)          | :heavy_check_mark: | :heavy_check_mark: |                    | :heavy_check_mark: |
| Ubuntu 18.04 (Bionic)         | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: | :heavy_check_mark: |
| Ubuntu 20.04 (Focal)          |                    | :heavy_check_mark: |                    | :heavy_check_mark: |
| Ubuntu 20.04 (Hirsute)        |                    | :heavy_check_mark: |                    | :heavy_check_mark: |
| Ubuntu 20.04 (Impish)         |                    | :heavy_check_mark: |                    | :heavy_check_mark: |

To add the repository, create a file /etc/apt/sources.list.d/hb9fxx.list containing the following two lines:

```
deb https://debian.hb9fxx.ch/debian/ buster/
deb-src https://debian.hb9fxx.ch/debian/ buster/
```

You must replace the 'buster' keyword with your distribution:
- Debian: 'stretch' 'buster' or bullseye
- Ubuntu/Mint: 'bionic', 'focal', 'hirsute' or 'impish'

Linux Mint can use Ubuntu packages, Linux Mint 19 needs 'bionic' and Linux Mint 20 needs 'focal'

Don't forget the trailing '/'. The debian keywords must not be changed, even if you use Ubuntu/Mint.

After having created this file, run:
```
sudo apt-get install apt-transport-https
wget https://debian.hb9fxx.ch/debian/key.asc -O - | sudo apt-key add -
sudo apt-get update
sudo apt-get install libsdrplay-api
```

If you want also the header files:
```
sudo apt-get install libsdrplay-api-dev
```
